let apiRoot = "http://localhost:3333/api"
export const apiHost = "http://localhost:3333"
let adminRoot = "http://localhost:3333/api/admin"
// let apiRoot = "http://192.168.165.27:3333/"
// let adminRoot = "http://192.168.165.27:3333/admin"
export async function Register(formData){
    try {
    let response = await fetch(`${apiRoot}/register`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}
export async function login(formData){
    try {
    let response = await fetch(`${apiRoot}/login`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getRegions(){
    try {
    let response = await fetch(`${adminRoot}/region`)
    return response
    } catch (error) {
        console.error(error);
    }
}
export async function createRegion(formData){
    try {
    let response = await fetch(`${adminRoot}/region`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getHouseTypes(){
    try {
    let response = await fetch(`${adminRoot}/house-type`)
    return response
    } catch (error) {
        console.error(error);
    }
}
export async function createHouseType(formData){
    try {
    let response = await fetch(`${adminRoot}/house-type`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getCities(){
    try {
    let response = await fetch(`${adminRoot}/city`)
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function createCity(formData){
    try {
    let response = await fetch(`${adminRoot}/city`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function createHouse(formData){
    try {
    let response = await fetch(`${adminRoot}/house`, {
        method: "POST",
        body: formData,
        headers: {
            'Accept': '*/*',
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE, HEAD',
            'Access-Control-Allow-Headers': 'origin,X-Requested-With,content-type,accept',
            'Access-Control-Allow-Credentials': 'true' 
        }
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getHouses(){
    try {
    let response = await fetch(`${adminRoot}/house`)
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getHouse(id){
    try {
    let response = await fetch(`${adminRoot}/house/${id}`)
    return response
    } catch (error) {
        console.error(error);
    }
}

// export async function searchHouses(region_id, city_id, house_type_id){
//     try {
//     let response = await fetch(`${adminRoot}/house/search`)
//     return response
//     } catch (error) {
//         console.error(error);
//     }
// }

export async function searchHouses(formData){
    try {
    let response = await fetch(`${adminRoot}/house/search`, {
        method: "POST",
        body: formData
    })
    return response
    } catch (error) {
        console.error(error);
    }
}

export async function getUsers(){
    try {
    let response = await fetch(`${adminRoot}/user`)
    return response
    } catch (error) {
        console.error(error);
    }
}
