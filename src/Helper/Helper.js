export function toFormData(json){
    let formData = new FormData()
    for (const key in json) {
        if (Object.hasOwnProperty.call(json, key)) {
            formData.append(key, json[key]);
        }
    }

    return formData;
}

// export function dateToFull(date){

// }