import { Apartment, Home, Login } from '@mui/icons-material'
import { AppBar, Avatar, Stack, Toolbar, Typography, Button, Box, Dialog, DialogTitle, DialogContent, TextField, DialogContentText } from '@mui/material'
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { login } from '../Api/Api'
import { toFormData } from '../Helper/Helper'
import authStore from '../Context/Auth.mobx'

const menuList = [
  {
    title: "Houses",
    icon: <Apartment />,
    to: "/"
  },
  {
    title: "Become owner",
    icon: <Home />,
    to: "/owner/create"
  },
  // {
  //   title: "Login",
  //   icon: <Home />,
  //   to: "/admin/dashboard"
  // },
]

const UserNavbar = () => {
  const [open, setopen] = useState(false)
  const [email, setemail] = useState("fru@test.com")
  const [password, setpassword] = useState("azertyuio")
  const navigate = useNavigate()
  let auth = authStore

  const handleClose = ()=>{
    setopen(false)
  }
  const handleLogin = async()=>{
    let formData = toFormData({email, password})
    let credentials = await login(formData)
    if(credentials.status == 200){
      credentials = await credentials.json()
      console.log(credentials);
      auth.setUser(credentials.user)
      auth.setUserToken(credentials.token.token)
      if(credentials.user.id === 1){
        navigate("/admin/dashboard")
      } else if(credentials.user.id === 2){
        navigate("/admin/dashboard")
      } else if(credentials.user.id === 3){
        navigate("/admin/dashboard")
      } else {
        navigate("/")
      }
    } else {
      alert("mot de passe ou email incorrect")
    }
  }
  return (
    <AppBar
      position="static"
      // sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px`, }}
    >
      <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Button variant='text' to={"/"} color="inherit" component={Link} >Estate System</Button>
          </Typography>
          <Box  sx={{display: {xs: "none", sm: "none", md: "block" }}}>
            {/* <TextField placeholder="Search product" /> */}
            {menuList.map((menu, i) => (

            <Button key={i} variant='text' to={menu.to} color="inherit" component={Link} startIcon={menu.icon} >{menu.title}</Button>
            ))}
          </Box>
          <Box sx={{display: {xs: "none", sm: "none", md: "block" }}}>
            <Button onClick={(e)=>{setopen(true)}} variant='text' color="inherit" startIcon={<Login />} >Login</Button>
          </Box>
          <Avatar />
      </Toolbar>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Login</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            label="email"
            type='email'
            fullWidth
            margin='dense'
            value={email}
            onChange={(e)=> setemail(e.target.value)}
          />
          <TextField
            label="Password"
            type='password'
            fullWidth
            margin='dense'
            value={password}
            onChange={(e)=> setpassword(e.target.value)}
          />
          <Stack mt={1}>
            <Button variant='contained' onClick={handleLogin}>Login</Button>
          </Stack>
          <DialogContentText>
            <br />
            Not yet have an account ? Contact and administrator
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </AppBar>
  )
}

export default UserNavbar