import React, {useState} from 'react'
import { 
    Box, 
    Drawer, 
    List, IconButton,
    ListItem, ListItemText, ListItemButton, ListItemIcon,
    Hidden,
    useTheme,
    Typography,
    Divider,
    Avatar
 } from '@mui/material'

import { Dashboard, HomeOutlined, Mail } from '@mui/icons-material'
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import authStore from '../Context/Auth.mobx';
import { toJS } from 'mobx';


const menuList = [
  {
    title: "Home",
    icon: <HomeOutlined />,
    to: "/",
    role: 2
  },
  {
    title: "Dashboard",
    icon: <Dashboard />,
    to: "/admin/dashboard",
    role: 2
  },
  {
    title: "Houses",
    icon: <Dashboard />,
    to: "/admin/houses",
    role: 2
  },
  {
    title: "House Types",
    icon: <Dashboard />,
    to: "/admin/house-type",
    role: 1
  },
  // {
  //   title: "Add House",
  //   icon: <Mail />,
  //   to: "/admin/houses/create"
  // },
  {
    title: "Owners",
    icon: <Dashboard />,
    to: "/admin/user",
    role: 1
  },
  {
    title: "Approve Owner",
    icon: <Dashboard />,
    to: "/admin/approve",
    role: 1
  },
  // {
  //   title: "Add owner",
  //   icon: <Mail />,
  //   to: "/admin/houses"
  // },
  {
    title: "Cities",
    icon: <Dashboard />,
    to: "/admin/city",
    role: 1
  },
  {
    title: "Regions",
    icon: <Dashboard />,
    to: "/admin/region",
    role: 1
  },
  // {
  //   title: "Settings",
  //   icon: <Mail />,
  //   to: "/admin/houses"
  // },
]

const drawerWidth = 240

const CustomDrawer = styled(Drawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
      boxSizing: 'border-box',
      // ...(open && {
      //   ...openedMixin(theme),
      //   '& .MuiDrawer-paper': openedMixin(theme),
      // }),
      // ...(!open && {
      //   ...closedMixin(theme),
      //   '& .MuiDrawer-paper': closedMixin(theme),
      // }),
    }),
    );

const Sidebar = () => {      
    const theme = useTheme()
    const dummyCategories = ['Hokusai', 'Hiroshige', 'Utamaro', 'Kuniyoshi', 'Yoshitoshi']
    const [mobileOpen, setMobileOpen] = useState(false)
    const [open, setOpen] = useState(false)
    let user = toJS(authStore.user)

  return (
    <>
        <CustomDrawer variant="permanent">
            <Box sx={{width: drawerWidth}}>
                <Box sx={{display: "flex", flexDirection: "column", alignItems: "center", pt: 4}}>
                    <Avatar sx={{width: 80, height: 80}} />
                    <Typography></Typography>
                    <Typography>Admin</Typography>
                </Box>
                <List>
                    {menuList.map((menu, index) => {return (menu.role >= user.role_id) ? (
                        <ListItem key={index} disablePadding>
                        <ListItemButton to={menu.to} component={Link}
                        sx={{
                            minHeight: 48,
                        }}>
                            <ListItemIcon
                            sx={{
                            minWidth: 0,
                            justifyContent: 'center',
                            px: 2.5,
                            }}>
                            {menu.icon}
                            </ListItemIcon>
                                <ListItemText primary={menu.title} />
                        </ListItemButton>
                        </ListItem>
                    ) : ("")})}
                </List>
            </Box>
        </CustomDrawer>
    </>
  )
}

export default Sidebar