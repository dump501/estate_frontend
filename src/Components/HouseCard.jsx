import { Card, CardActionArea, CardContent, CardMedia, Typography, Stack } from '@mui/material'
import React from 'react'
import data from '../data'
import { Link } from 'react-router-dom'
import { ClassOutlined, SquareFootOutlined, LocationOnOutlined } from '@mui/icons-material'
import { apiHost } from '../Api/Api'

const HouseCard = ({house}) => {
  return (
    <Card>
        <CardActionArea LinkComponent={Link} to={`/houses/${house.id}`}>
            <CardMedia component="img" image={apiHost + house.image} />
        <CardContent>
          <Typography variant='h5'>{house.name}</Typography>
          <Stack direction="row">
            <Typography variant='body2'>{house.houseType.name}</Typography>&nbsp;&nbsp;&nbsp;
            <Typography variant='body2'>{house.width}m x {house.length}m</Typography>
          </Stack>
          <Stack direction="row">
            <Typography variant='body2'>{house.region.name} || {house.city.name} || {house.quater}</Typography>
          </Stack>
            <Typography variant='body2'>{house.mounth_price} FCFA / Mounth</Typography>
            <Typography variant='body2'>{house.year_price} FCFA / Year </Typography>
        </CardContent>
        </CardActionArea>
    </Card>
  )
}

export default HouseCard