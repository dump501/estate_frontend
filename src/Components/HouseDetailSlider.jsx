import { Box } from '@mui/material'
import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import data from '../data';

const PrevArrow = (props)=>{
    const { className, style, onClick } = props;
    return (
        <div
        className={className}
        style={{ ...style, display: "block", background: "#1976d2", borderRadius: "100%" }}
        onClick={onClick} />
    )
}

const NextArrow = (props)=>{
    const { className, style, onClick } = props;
    return (
        <div
        className={className}
        style={{ ...style, display: "block", background: "#1976d2", borderRadius: "100%" }}
        onClick={onClick} />
    )
}

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    // slidesToShow: 2,
    // slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 400,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            initialSlide: 2,
            infinite: true,
            dots: true
        }
      }
    ]
};
const imageStyle = {
    height: "auto",
    width: "100%"
}
const HouseDetailSlider = ({image1,image2,image3,image4}) => {
  return (
    <Box sx={{px: 4}}>
      <Slider {...settings}>
          <div>
              <img style={imageStyle} src={image1} />
          </div>
          <div>
              <img style={imageStyle} src={image2} />
          </div>
          <div>
              <img style={imageStyle} src={image3} />
          </div>
          <div>
              <img style={imageStyle} src={image4} />
          </div>
      </Slider>
    </Box>
  )
}

export default HouseDetailSlider