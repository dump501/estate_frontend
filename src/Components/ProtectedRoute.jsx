import { Route } from "react-router-dom";
import auth from "../Context/Auth.mobx";

export const ProtectedRoute = ({...rest }) => {
    let { user } = auth.user
  
    if (!user || !user.token || user.token === "") {
      return (
        // component which inform the user that they must be logged in
        <h1>You must login</h1>
      );
    }
  
    // let user through if they're logged in
    return <Route {...rest} />;
  };