import { AppBar, Avatar, Stack, Toolbar, Typography, Button } from '@mui/material'
import React from 'react'

const drawerWidth = 240

const Navbar = () => {
  return (
    <AppBar
      position="fixed"
      sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px`, }}
    >
        <Toolbar>
          <Button variant='text' color="inherit"><Avatar /></Button>
        </Toolbar>
    </AppBar>
  )
}

export default Navbar