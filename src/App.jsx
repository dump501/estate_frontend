import { useState } from 'react'
import './App.css'
import { Box, Drawer, Stack } from '@mui/material'
import Sidebar from './Components/Sidebar'
import Navbar from './Components/Admin/Navbar'
import CustomRoutes from './Components/CustomRoutes'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import Dashboard from './Pages/Admin/Dashboard'
import UserNavbar from './Components/UserNavbar'
import Home from './Pages/Home'
import HouseAdd from './Pages/Admin/HouseAdd'
import HouseList from './Pages/Admin/HouseList'
import HouseDetails from './Pages/HouseDetails'
import OwnerAdd from './Pages/OwnerAdd'
import City from './Pages/Admin/City'
import Region from './Pages/Admin/Region'
import HouseType from './Pages/Admin/HouseType'
import SearchResult from './Pages/SearchResult'
import OwnerList from './Pages/Admin/OwnerList'
import Approve from './Pages/Admin/Approve'

function App() {

  return (
    <>
      <Router>
        <CustomRoutes>
          <Route path="/" element={<Home />} />
          <Route path="/admin">
            <Route path='dashboard' element={<Dashboard />} />
            <Route path='houses' element={<HouseList />} />
            <Route path='houses/create' element={<HouseAdd />} />
            <Route path='house-type' element={<HouseType />} />
            <Route path='city' element={<City />} />
            <Route path='region' element={<Region />} />
            <Route path='user' element={<OwnerList />} />
            <Route path='approve' element={<Approve />} />
          </Route>
          <Route path='houses/:id' element={<HouseDetails />} />
          <Route path='owner/create' element={<OwnerAdd />} />
          <Route path='result/:region/:city/:houseType' element={<SearchResult />} />
        </CustomRoutes>
      </Router>
    </>
  )
}

export default App
