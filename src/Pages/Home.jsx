import React, { useEffect, useState } from 'react'
import UserLayout from './UserLayout'
import { Box, Grid, Stack, Select, TextField,InputAdornment, Button, Typography, Divider, MenuItem } from '@mui/material'
import { Search } from '@mui/icons-material'
import HouseCard from '../Components/HouseCard'
import { getCities, getRegions, getHouseTypes, getHouses, searchHouses } from '../Api/Api'
import { useNavigate } from 'react-router-dom'

// const housesDummy = [
//   {
//     lenght: 7,
//     width: 8,
//     city: "Bambili",
//     region: "North-ouest",
//     quater: "mile 4",
//     type: "Studio",
//     yearPrice: 100000,
//     monthPrice: 5000
//   },
//   {
//     lenght: 7,
//     width: 8,
//     city: "Bamenda",
//     region: "North-ouest",
//     quater: "mile 4",
//     type: "Studio",
//     yearPrice: 100000,
//     monthPrice: 5000
//   },
//   {
//     lenght: 7,
//     width: 8,
//     city: "Bamenda",
//     region: "North-ouest",
//     quater: "mile 4",
//     type: "Studio",
//     yearPrice: 100000,
//     monthPrice: 5000
//   },
//   {
//     lenght: 7,
//     width: 8,
//     city: "Bamenda",
//     region: "North-ouest",
//     quater: "mile 4",
//     type: "Studio",
//     yearPrice: 100000,
//     monthPrice: 5000
//   },
//   {
//     lenght: 7,
//     width: 8,
//     city: "Bamenda",
//     region: "North-ouest",
//     quater: "mile 4",
//     type: "Studio",
//     yearPrice: 100000,
//     monthPrice: 5000
//   },
// ]

const Home = () => {
  const [houses, sethouses] = useState(null)
  const [region, setregion] = useState(1)
  const [city, setcity] = useState(1)
  const [regions, setregions] = useState(null)
  const [cities, setcities] = useState(null)
  const [houseTypes, sethouseTypes] = useState(null)
  const [houseType, sethouseType] = useState(1)
  let navigate = useNavigate()

  const handleSearch = async()=>{
    // let formData = new FormData()
    // formData.append("region_id", region)
    // formData.append("city_id", city)
    // formData.append("house_type_id", houseType)
    // let response = await searchHouses(formData)
    // response = await response.json()
    // console.log(response);
    navigate(`/result/${region}/${city}/${houseType}`)
  }

  useEffect(()=>{
    const fetchData = async()=>{
      let response = await getHouses()
      response = await response.json()
      sethouses(response.data)
      console.log(response);
      let cities = await getCities()
      cities = await cities.json()
      setcities(cities.data)
      let regions = await getRegions()
      regions = await regions.json()
      setregions(regions.data)
      let houseTypes = await getHouseTypes()
      houseTypes = await houseTypes.json()
      sethouseTypes(houseTypes.data)
    }
    fetchData()
  }, [])

  return (
    <UserLayout>
      <Grid container>
        <Grid item sm={12}>
          <Box>
            <Stack direction={{xs: "column", md: "row"}} spacing={2} sx={{display: "flex", flexDirection: {sm: "column", md:"row"}}}>
                
                {regions && <Select value={region} onChange={(e)=>{setregion(e.target.value)}} fullWidth label="Region" variant="outlined" >
                  {regions.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
                
                {cities && <Select value={city} onChange={(e)=>{setcity(e.target.value)}} fullWidth label="City" variant="outlined" >
                  {cities.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
                
                {houseTypes && <Select value={houseType} onChange={(e)=>{sethouseType(e.target.value)}} fullWidth label="House type" variant="outlined" >
                  {houseTypes.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
                <Box sx={{display: "flex", alignItems: "center"}}>
                  <Button
                    edge="end"
                    startIcon={<Search />}
                    onClick={handleSearch}
                    variant="contained"
                  >
                    Search
                  </Button>
                </Box>
            </Stack>
          </Box>
        </Grid>
      </Grid>
      <Typography variant="h4">Latest Houses</Typography>
      <Divider /><br />
      <Grid container spacing={3}>
        {houses && houses.map((house, i) => (
          <Grid key={i} item md={3} sm={12}>
            <HouseCard house={house} />
          </Grid>
        ))}
      </Grid>
    </UserLayout>
  )
}

export default Home