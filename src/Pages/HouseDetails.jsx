import { Avatar, Box, Button, Container, Grid, Stack, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import data from '../data'
import UserLayout from './UserLayout'
import HouseDetailSlider from '../Components/HouseDetailSlider'
import {SquareFootOutlined, ClassOutlined, LocationOnOutlined, WhatsApp, MailOutlined, Message } from '@mui/icons-material'
import { useParams } from 'react-router-dom'
import { apiHost, getHouse } from '../Api/Api'

// const house = {
//   lenght: 7,
//   width: 8,
//   city: "Bamenda",
//   region: "North-ouest",
//   quater: "mile 4",
//   type: "Studio"
// }
const HouseDetails = () => {
  let params = useParams()
  const [house, sethouse] = useState(null)

  useEffect(()=>{
    const fetchData = async()=>{
      console.log(params);
      let response = await getHouse(params.id)
      response = await response.json()
      sethouse(response.data)
      console.log(response);
    }
    fetchData()
  }, [])
  return (
      <UserLayout>
          {house && <Container>
              <Grid container spacing={2}>
                  <Grid item md={6} sm={12} xs={12}>
                    <img style={{width: "100%", height: "auto"}} src={apiHost + house.image} /><br /><br />
                    <Box>
                      <HouseDetailSlider image1={apiHost + house.image1} image2={apiHost + house.image2} image3={apiHost + house.image3} image4={apiHost + house.image} />
                    </Box>
                  </Grid>
                  <Grid item md={6} sm={12} xs={12}>
                    <Stack pt={2}>
                    <Typography variant='h5'>{house.name}</Typography>
                      <Typography variant='h5'>{house.houseType.name} {house.width}m x {house.length}m</Typography>
                      <Typography variant='h6'>{house.region.name} || {house.city.name} || {house.quater}</Typography>
                      <Typography variant='h6'>{house.year_price} FCFA / year || {house.mounth_price} FCFA / Mounth</Typography>
                      <Typography>Precise location: {house.location}</Typography><br /><br />
                      <Typography variant='h6'>{house.description}</Typography>
                    </Stack><br />
                    <Typography>Owner</Typography>
                    <Stack direction={{sm: "column", md:"row"}} spacing={3} pt={4}>
                      {house?.user?.avatar ? <img style={{width: "auto", height: 100}} src={house.user.avatar} /> : <Avatar sx={{width: 80, height: 80}} />}
                      <Box>
                        <Typography>{house.user.name}</Typography>
                        <Stack direction="row" spacing={1}>
                          <MailOutlined color="error" />
                          <Typography>{house.user.email}</Typography>
                        </Stack>
                        <Stack direction="row" spacing={1}>
                          <WhatsApp color="success" mt={1} />
                          <Typography>{house.user.phone}</Typography>
                        </Stack>
                      </Box>
                    </Stack>
                    <Typography>{house.user.description}</Typography><br /><br />
                    <Button startIcon={<Message />} variant="contained">Tchat with Alex parrish</Button>
                  </Grid>
              </Grid>
          </Container>}
      </UserLayout>
  )
}

export default HouseDetails