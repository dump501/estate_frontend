import { Box, Grid, Paper, Stack, Typography } from '@mui/material'
import React, {useEffect, useState} from 'react'
import AdminLayout from './AdminLayout'
import { DataGrid } from '@mui/x-data-grid';
import { Apartment, Home, LocationOn, Map, Person } from '@mui/icons-material'
import { getCities, getHouses, getRegions, getUsers } from '../../Api/Api';


const columns = [
  { field: 'houseType', headerName: 'House type' },
  { field: 'city', headerName: 'City' },
  { field: 'region', headerName: 'Region' },
  { field: 'length', headerName: 'Length' },
  { field: 'width', headerName: 'Width' },
  { field: 'owner', headerName: 'Owner' },
];

const usercolumns = [
  { field: 'name', headerName: 'name' },
  { field: 'email', headerName: 'email' },
  { field: 'phone', headerName: 'phone' },
];


const Dashboard = () => {
  const [rows, setrows] = useState([])
  const [userrows, setuserrows] = useState([])
  const [ownernum, setownernum] = useState(0)
  const [housenum, sethousenum] = useState(0)
  const [citynum, setcitynum] = useState(0)
  const [regionnum, setregionnum] = useState(0)

  useEffect(()=>{
    let fetchData = async()=>{
      let houses = await getHouses()
      houses = await houses.json()
      console.log(houses.data);

      sethousenum(await houses.data.length)
      let cols = []
      houses.data.map((house, i) => {
        if(i<5){

        cols.push({
          id: house.id, 
          houseType: house.houseType.name, 
          city: house.city.name, 
          region: house.region.name,
          length: house.length,
          width: house.width,
          owner: house.user.name
        })
        }
      })
      setrows(cols)
      houses = await getUsers()
      houses = await houses.json()
      console.log(houses.data);
      setownernum(await houses.data.length)
      cols = []
      houses.data.map((house, i) => {
        if(i<5){
          cols.push({
            id: house.id, 
            name: house.name, 
            email: house.email, 
            phone: house.phone,
          })
        }
      })
      setuserrows(cols)
      let regions = await getRegions()
      regions = await regions.json()
      setregionnum(regions.data.length)
      let cities = await getCities()
      cities = await cities.json()
      setcitynum(cities.data.length)
    }
    fetchData()
  }, [])
  
  return (
    <AdminLayout>
      <Stack direction="row" spacing={2}>
        <Paper elevation={0} sx={{ backgroundColor: "primary.main", color: "common.white", py: 2, px: 8 }}>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <Apartment color="white" sx={{ fontSize: 70 }} />
            <Box>
              <Typography variant="h5">{housenum}</Typography>
              <Typography variant="subtitle2">Houses</Typography>
            </Box>
          </Box>
        </Paper>
        <Paper elevation={0} sx={{ backgroundColor: "primary.main", color: "common.white", py: 2, px: 8 }}>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <Person color="white" sx={{ fontSize: 70 }} />
            <Box>
              <Typography variant="h5">{ownernum}</Typography>
              <Typography variant="subtitle2">Owners</Typography>
            </Box>
          </Box>
        </Paper>
        <Paper elevation={0} sx={{ backgroundColor: "primary.main", color: "common.white", py: 2, px: 8 }}>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <LocationOn color="white" sx={{ fontSize: 70 }} />
            <Box>
              <Typography variant="h5">{citynum}</Typography>
              <Typography variant="subtitle2">Cities</Typography>
            </Box>
          </Box>
        </Paper>
        <Paper elevation={0} sx={{ backgroundColor: "primary.main", color: "common.white", py: 2, px: 8 }}>
          <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
            <Map color="white" sx={{ fontSize: 70 }} />
            <Box>
              <Typography variant="h5">{regionnum}</Typography>
              <Typography variant="subtitle2">Regions</Typography>
            </Box>
          </Box>
        </Paper>
      </Stack>
      <Stack direction="row" spacing={2} sx={{pt: 2}}>
        <DataGrid
        rows={rows}
        columns={columns}/>
        <DataGrid
        rows={userrows}
        columns={usercolumns}/>
      </Stack>
    </AdminLayout>
  )
}

export default Dashboard