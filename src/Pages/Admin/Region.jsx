import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Stack, Typography, Dialog, DialogTitle,DialogContent,TextField,Button } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import {createRegion, getRegions} from '../../Api/Api'
import { useNavigate } from 'react-router-dom';


const columns = [
  { field: 'name', headerName: 'Name'  }
];


const Region = () => {
  const [open, setopen] = useState(false)
  const [rows, setrows] = useState(null)
  const [name, setname] = useState("")
  let naviguate = useNavigate()
  const handleClose = ()=>{
    setopen(false)
  }
  const addRegion = async()=>{
    if(name){
      let formData = new FormData()
      formData.append("name", name)
      let region = createRegion(formData)
      region = await region.json()
      setopen(false)
      naviguate("/admin/dashboard")
    }
  }
  useEffect(()=>{
    let fetchData = async()=>{
      let response = await getRegions()
      response = await response.json()
      console.log(response);
      setrows(response.data)
    }
    fetchData()
  }, [])
  return (
    <AdminLayout>
      <Stack direction="row" sx={{justifyContent: "space-between"}}>
        <Typography>Region List</Typography>
        <Button variant="contained" onClick={(e)=>{setopen(true)}}>Add region</Button>
      </Stack>
      <Stack>
        {rows && <DataGrid
          rows={rows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 9 },
            },
          }}
          pageSizeOptions={[10]} />}
      </Stack>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Register</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            label="Name"
            type="text"
            fullWidth
            margin="dense"
            onChange={(e)=>{setname(e.target.value)}}
          />
          <Stack mt={1}>
            <Button variant="contained" onClick={addRegion}>Add region</Button>
          </Stack>
        </DialogContent>
      </Dialog>
    </AdminLayout>
  )
}

export default Region