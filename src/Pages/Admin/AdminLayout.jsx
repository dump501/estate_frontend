import React from 'react'
import Sidebar from '../../Components/Sidebar'
import Navbar from '../../Components/Admin/Navbar'
import { Stack, Box } from '@mui/material'

const AdminLayout = ({children}) => {
  return (
    <Stack direction="row">
        <Sidebar />
        <Box sx={{width: "100%"}}>
            <Navbar />
            <Box sx={{height: 64}}></Box>
            <Box sx={{ml: 2, p: 2}}>{children}</Box>
        </Box>
    </Stack>
  )
}

export default AdminLayout