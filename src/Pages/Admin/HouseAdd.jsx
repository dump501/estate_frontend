import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Stack, TextField, Typography, MenuItem, Select, Grid, Button, TextareaAutosize } from '@mui/material'
import { Add } from '@mui/icons-material'
import { createHouse, getCities, getHouseTypes, getRegions } from '../../Api/Api'
import { useNavigate } from 'react-router-dom'

const HouseAdd = () => {
  const [name, setname] = useState("")
  const [description, setdescription] = useState("")
  const [length, setlength] = useState(0)
  const [width, setwidth] = useState(0)
  const [mounthPrice, setmounthPrice] = useState(0)
  const [yearPrice, setyearPrice] = useState(0)
  const [region, setregion] = useState(1)
  const [city, setcity] = useState(1)
  const [image, setimage] = useState(1)
  const [image1, setimage1] = useState(null)
  const [image2, setimage2] = useState(null)
  const [image3, setimage3] = useState(null)
  const [image4, setimage4] = useState(null)
  const [regions, setregions] = useState(null)
  const [cities, setcities] = useState(null)
  const [houseTypes, sethouseTypes] = useState(null)
  const [houseType, sethouseType] = useState(1)
  const [location, setlocation] = useState("beside CamCul")
  const [quater, setquater] = useState("mile 4")
  let navigate = useNavigate()

  const addHouse = async()=>{
    if(
      name && description && length && width && mounthPrice && yearPrice && region
      && city && image && image1 && image2 && image3 && image4 && houseType && location
      && quater
    ){
      let formData = new FormData()
      formData.append("name", name)
      formData.append("description", description)
      formData.append("length", length)
      formData.append("width", width)
      formData.append("mounthPrice", mounthPrice)
      formData.append("yearPrice", yearPrice)
      formData.append("region_id", region)
      formData.append("city_id", city)
      formData.append("image", image)
      formData.append("image1", image1)
      formData.append("image2", image2)
      formData.append("image3", image3)
      formData.append("image4", image4)
      formData.append("user_id", 1)
      formData.append("house_type_id", houseType)
      formData.append("location", location)
      formData.append("quater", quater)
      let house = await createHouse(formData)
      // await upload(formData)
      console.log(house);
      navigate("/admin/houses")
    } else {
      alert("please fill out all the form fields")
    }
  }

  useEffect(()=>{
    let fetchData = async()=>{
      let cities = await getCities()
      cities = await cities.json()
      setcities(cities.data)
      let response = await getRegions()
      response = await response.json()
      setregions(response.data)
      let houseTypes = await getHouseTypes()
      houseTypes = await houseTypes.json()
      sethouseTypes(houseTypes.data)
    }
    fetchData()
  }, [])
  return (
    <AdminLayout>
        <Typography variant="h4">Add a new house</Typography>
        <Stack pt={4} spacing={2}>
            <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                <TextField type="number" value={width} onChange={(e)=>setwidth(e.target.value)} fullWidth label="Width" variant="outlined" />
                <TextField type="number" value={length} onChange={(e)=>setlength(e.target.value)} fullWidth label="Length" variant="outlined" />
            </Stack>
            <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                <TextField type="number" value={yearPrice} onChange={(e)=>setyearPrice(e.target.value)} fullWidth label="year price" variant="outlined" />
                <TextField type="number" value={mounthPrice} onChange={(e)=>setmounthPrice(e.target.value)} fullWidth label="mountn price" variant="outlined" />
            </Stack>
            <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                
                {regions && <Select value={region} onChange={(e)=>{setregion(e.target.value)}} fullWidth label="Region" variant="outlined" >
                  {regions.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
                
                {cities && <Select value={city} onChange={(e)=>{setcity(e.target.value)}} fullWidth label="City" variant="outlined" >
                  {cities.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
            </Stack>
            <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                <TextField type="text" value={quater} onChange={(e)=>setquater(e.target.value)} fullWidth label="Quater" variant="outlined" />
                <TextField type="text" value={location} onChange={(e)=>setlocation(e.target.value)} fullWidth label="location explanation" variant="outlined" />
            </Stack>
            <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                {houseTypes && <Select value={city} onChange={(e)=>{sethouseType(e.target.value)}} fullWidth label="House type" variant="outlined" >
                  {houseTypes.map((reg, i) => <MenuItem key={i} value={reg.id}>{reg.name}</MenuItem>)}
                </Select>}
                <TextField type="text" value={name} onChange={(e)=>setname(e.target.value)} fullWidth label="City name" variant="outlined" />
            </Stack>
            <TextareaAutosize minRows={6} value={description} onChange={(e)=>{setdescription(e.target.value)}} placeholder='Description' style={{padding: "1em", borderRadius: ".5em"}} />
            <Grid container>
              <Grid item md={6}>
                <Stack direction="row" spacing={2} my={2}>
                  <Typography>Main Image</Typography>
                  <input type="file" onChange={(e)=>{setimage(e.target.files[0])}} />
                </Stack>
                <Stack direction="row" spacing={2} my={2}>
                  <Typography>Image1</Typography>
                  <input type="file" onChange={(e)=>{setimage1(e.target.files[0])}} />
                </Stack>
                <Stack direction="row" spacing={2} my={2}>
                  <Typography>Image2</Typography>
                  <input type="file" onChange={(e)=>{setimage2(e.target.files[0])}} />
                </Stack>
              </Grid>
              <Grid item md={6}>
                <Stack direction="row" spacing={2} my={2}>
                  <Typography>Image3</Typography>
                  <input type="file" onChange={(e)=>{setimage3(e.target.files[0])}} />
                </Stack>
                <Stack direction="row" spacing={2} my={2}>
                  <Typography>Image4</Typography>
                  <input type="file" onChange={(e)=>{setimage4(e.target.files[0])}} />
                </Stack>
              </Grid>
            </Grid>
                <Button onClick={addHouse} startIcon={<Add />} variant="contained">Add house</Button>
        </Stack>
    </AdminLayout>
  )
}

export default HouseAdd