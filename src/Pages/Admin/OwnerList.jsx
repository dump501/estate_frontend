import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Box, Button, Stack, Typography } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { getHouses, getUsers } from '../../Api/Api';

const columns = [
    { field: 'name', headerName: 'name' },
    { field: 'email', headerName: 'email' },
    { field: 'phone', headerName: 'phone' },
  ];

const OwnerList = () => {
    const [rows, setrows] = useState([])
    useEffect(()=>{
      let fetchData = async()=>{
        let houses = await getUsers()
        houses = await houses.json()
        console.log(houses.data);
        let cols = []
        houses.data.map((house) => {
          cols.push({
            id: house.id, 
            name: house.name, 
            email: house.email, 
            phone: house.phone,
          })
        })
        setrows(cols)
      }
      fetchData()
    }, [])
    return (
      <AdminLayout>
        <Stack direction="row" sx={{justifyContent: "space-between"}}>
          <Typography variant="h4">Owner list</Typography>
        </Stack>
        <DataGrid
          rows={rows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 9 },
            },
          }}
          pageSizeOptions={[10]}/>
      </AdminLayout>
    )
}

export default OwnerList