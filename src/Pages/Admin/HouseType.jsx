import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Stack, Typography, Dialog, DialogTitle,DialogContent,TextField,Button } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import {createHouseType, getHouseTypes} from '../../Api/Api'
import { useNavigate } from 'react-router-dom';

const columns = [
    { field: 'name', headerName: 'Name'  }
  ];

const HouseType = () => {
    const [open, setopen] = useState(false)
    const [rows, setrows] = useState(null)
    const [name, setname] = useState("")
    let naviguate = useNavigate()
    const handleClose = ()=>{
      setopen(false)
    }
    const addRegion = async()=>{
      if(name){
        let formData = new FormData()
        formData.append("name", name)
        let region = await createHouseType(formData)
        region = await region.json()
        setopen(false)
        naviguate("/admin/dashboard")
      }
    }
    useEffect(()=>{
      let fetchData = async()=>{
        let response = await getHouseTypes()
        response = await response.json()
        console.log(response);
        setrows(response.data)
      }
      fetchData()
    }, [])
    return (
      <AdminLayout>
        <Stack direction="row" sx={{justifyContent: "space-between"}}>
          <Typography>House type List</Typography>
          <Button variant="contained" onClick={(e)=>{setopen(true)}}>Add House type</Button>
        </Stack>
        <Stack>
          {rows && <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: { page: 0, pageSize: 9 },
              },
            }}
            pageSizeOptions={[10]} />}
        </Stack>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Add House type</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              label="Name"
              type="text"
              fullWidth
              margin="dense"
              onChange={(e)=>{setname(e.target.value)}}
            />
            <Stack mt={1}>
              <Button variant="contained" onClick={addRegion}>Add House type</Button>
            </Stack>
          </DialogContent>
        </Dialog>
      </AdminLayout>
    )
}

export default HouseType