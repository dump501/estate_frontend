import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Stack, Typography, Dialog, DialogTitle,DialogContent,TextField,Button, MenuItem, Select } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import {createCity, createRegion, getCities, getRegions} from '../../Api/Api'
import { useNavigate } from 'react-router-dom';


const columns = [
  { field: 'name', headerName: 'Name'  },
  { field: 'region', headerName: 'Region'  },
];

const City = () => {
  const [open, setopen] = useState(false)
  const [rows, setrows] = useState(null)
  const [name, setname] = useState("")
  const [region, setregion] = useState(null)
  const [regions, setregions] = useState(null)
  let naviguate = useNavigate()
  const handleClose = ()=>{
    setopen(false)
  }
  const addCity = async()=>{
    if(name && region){
      let formData = new FormData()
      formData.append("name", name)
      formData.append("region_id", region)
      let city = await createCity(formData)
      city = await city.json()
      setopen(false)
      naviguate("/admin/dashboard")
    }
  }
  useEffect(()=>{
    let fetchData = async()=>{
      let cities = await getCities()
      cities = await cities.json()
      console.log(cities);
      let response = await getRegions()
      response = await response.json()
      setregions(response.data)
      console.log(response);
      let cols = []
      cities.data.map((city) => {
        cols.push({id: city.id, name: city.name, region: city.region.name})
      })
      setrows(cols)
    }
    fetchData()
  }, [])
  return (
    <AdminLayout>
      <Stack direction="row" sx={{justifyContent: "space-between"}}>
        <Typography>Region List</Typography>
        <Button variant="contained" onClick={(e)=>{setopen(true)}}>Add City</Button>
      </Stack>
      <Stack>
        {rows && <DataGrid
          rows={rows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 9 },
            },
          }}
          pageSizeOptions={[10]} />}
      </Stack>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Register</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            label="Name"
            type="text"
            fullWidth
            margin="dense"
            onChange={(e)=>{setname(e.target.value)}}
          />
          {regions && <Select value={region} onChange={(e)=>{setregion(e.target.value)}} fullWidth label="Civility" variant="outlined" >
            {regions.map((reg) => <MenuItem value={reg.id}>{reg.name}</MenuItem>)}
          </Select>}
          <Stack direction="row" mt={1}>
            <Button variant="contained" onClick={addCity}>Add region</Button>
            <Button color='error' variant="contained" onClick={handleClose}>cancel</Button>
          </Stack>
        </DialogContent>
      </Dialog>
    </AdminLayout>
  )
}

export default City