import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Box, Button, Stack, Typography } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { getHouses } from '../../Api/Api';


const columns = [
  { field: 'houseType', headerName: 'House type' },
  { field: 'city', headerName: 'City' },
  { field: 'region', headerName: 'Region' },
  { field: 'length', headerName: 'Length' },
  { field: 'width', headerName: 'Width' },
  { field: 'owner', headerName: 'Owner' },
  { field: 'name', headerName: 'Name' },
];

// const rows = [
//   { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
//   { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
//   { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
//   { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
//   { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
//   { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
//   { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
//   { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
//   { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
// ];

const HouseList = () => {
  const [rows, setrows] = useState([])
  useEffect(()=>{
    let fetchData = async()=>{
      let houses = await getHouses()
      houses = await houses.json()
      console.log(houses.data);
      let cols = []
      houses.data.map((house) => {
        cols.push({
          id: house.id, 
          houseType: house.houseType.name, 
          city: house.city.name, 
          region: house.region.name,
          length: house.length,
          width: house.width,
          name: house.name,
          owner: house.user.name,
        })
      })
      setrows(cols)
    }
    fetchData()
  }, [])
  return (
    <AdminLayout>
      <Stack direction="row" sx={{justifyContent: "space-between"}}>
        <Typography variant="h4">House list</Typography>
        <Button variant="contained" to="/admin/houses/create" component={Link}>Add House</Button>
      </Stack>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 9 },
          },
        }}
        pageSizeOptions={[10]}/>
    </AdminLayout>
  )
}

export default HouseList