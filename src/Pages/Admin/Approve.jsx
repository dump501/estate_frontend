import React, { useEffect, useState } from 'react'
import AdminLayout from './AdminLayout'
import { Box, Button, MenuItem, Select, Stack, Typography } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { getHouses, getUsers } from '../../Api/Api';

const columns = [
    { field: 'name', headerName: 'name' },
    { field: 'email', headerName: 'email' },
    { field: 'phone', headerName: 'phone' },
    { field: 'status', headerName: 'status' },
  ];

const Approve = () => {
    const [rows, setrows] = useState([])
    useEffect(()=>{
      let fetchData = async()=>{
        let houses = await getUsers()
        houses = await houses.json()
        console.log(houses.data);
        let cols = []
        houses.data.map((house) => {
          cols.push({
            id: house.id, 
            name: house.name, 
            email: house.email, 
            phone: house.phone, 
            status: (
                
                <Select value={house.is_valid} onChange={(e)=>{e.target.value=e.target.value}} fullWidth label="approve" variant="outlined" >
                <MenuItem value={"1"}>Approve</MenuItem>
                <MenuItem value={"0"}>Disapprove</MenuItem>
              </Select>
            )
          })
        })
        setrows(cols)
      }
      fetchData()
    }, [])
    return (
      <AdminLayout>
        <Stack direction="row" sx={{justifyContent: "space-between"}}>
          <Typography variant="h4">Owner list</Typography>
        </Stack>
        <Box style={{width: "100%"}}>  
            <table style={{width: "100%"}}>
                <thead>
                    <td>name</td>
                    <td>email</td>
                    <td>phone</td>
                    <td>status</td>
                </thead>
                {rows && rows.map((item) => (<tr>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                    <td>{item.status}</td>
                </tr>))}
            </table>
        </Box>
        {/* <DataGrid
          rows={rows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 9 },
            },
          }}
          pageSizeOptions={[10]}/> */}
      </AdminLayout>
    )
}

export default Approve
