import React, {useState, useEffect} from 'react'
import UserLayout from './UserLayout'
import { MenuItem, Button, Container, TextareaAutosize } from '@mui/material'
import { Stack, TextField, Typography, Select } from '@mui/material'
import { Register } from '../Api/Api'
import authStore from '../Context/Auth.mobx'
import { observer } from 'mobx-react'
import { useNavigate } from 'react-router-dom'


const OwnerAdd = observer(() => {
  const [name, setname] = useState("")
  const [description, setdescription] = useState("")
  const [email, setemail] = useState("")
  const [telephone, settelephone] = useState("")
  const [civility, setcivility] = useState("")
  const [password, setpassword] = useState("")
  let naviguate = useNavigate()

  const handleRegister = async()=>{
    let formData = new FormData()
    formData.append("name", name)
    formData.append("email", email)
    formData.append("phone", telephone)
    formData.append("civility", civility)
    formData.append("password", password)
    formData.append("description", description)
    let response = await Register(formData)
    if(response.status == 200){
      response = await response.json()
      authStore.setUser(response.user)
      authStore.setUserToken(response.token)
      naviguate("/")
    }
  }

  return (
    <UserLayout>
        <Container>
            <Typography variant="h4">Owner registration form</Typography>
            <Stack pt={4} spacing={2}>
              <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                <TextField label="Name" value={name} onChange={(e)=>{setname(e.target.value)}} fullWidth variant="outlined" />
                <TextField value={email} onChange={(e)=>{setemail(e.target.value)}} fullWidth label="Email" variant="outlined" />
              </Stack>
                <Stack direction="row" spacing={2} sx={{width: "100%"}}>
                    <TextField value={telephone} onChange={(e)=>{settelephone(e.target.value)}} fullWidth label="Telephone" variant="outlined" />
                    <Select value={civility} onChange={(e)=>{setcivility(e.target.value)}} fullWidth label="Civility" variant="outlined" >
                      <MenuItem value={"Mr"}>Mr</MenuItem>
                      <MenuItem value={"Mss"}>Mss</MenuItem>
                    </Select>
                </Stack>
                <TextField value={password} onChange={(e)=>{setpassword(e.target.value)}} type="password" label="Password" variant="outlined" />
                <TextareaAutosize minRows={6} value={description} onChange={(e)=>{setdescription(e.target.value)}} placeholder='Description' style={{padding: "1em", borderRadius: ".5em"}} />
            </Stack><br />
            <Button onClick={handleRegister} size='large' variant="contained">Register</Button>
        </Container>
    </UserLayout>
  )
})

export default OwnerAdd