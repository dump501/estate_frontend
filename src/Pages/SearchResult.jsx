import React, {useEffect, useState} from 'react'
import UserLayout from './UserLayout'
import { Divider, Grid, Typography } from '@mui/material'
import HouseCard from '../Components/HouseCard'
import { searchHouses } from '../Api/Api'
import { useParams } from 'react-router-dom'

const SearchResult = () => {
  let params = useParams()
  const [houses, sethouses] = useState(null)

  useEffect(()=>{
    const fetchData = async()=>{
      let formData = new FormData()
      formData.append("region_id", params.region)
      formData.append("city_id", params.city)
      formData.append("house_type_id", params.houseType)
      let response = await searchHouses(formData)
      response = await response.json()
      console.log(response);
      sethouses(response.data.cities)
    }
    fetchData()
  }, [])


  return (
    <UserLayout>
      <Typography variant="h4">Search result</Typography>
      <Divider /><br />
      <Grid container spacing={3}>
        {houses && houses.map((house, i) => (
          <Grid key={i} item md={3} sm={12}>
            <HouseCard house={house} />
          </Grid>
        ))}
      </Grid>
    </UserLayout>
  )
}

export default SearchResult