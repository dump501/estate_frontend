import { Box } from '@mui/material'
import React from 'react'
import UserNavbar from '../Components/UserNavbar'

const UserLayout = ({children}) => {
  return (
    <Box sx={{width: "100%"}}>
        <UserNavbar />
        <Box sx={{width: "100%", p: 3}}>{children}</Box>
    </Box>
  )
}

export default UserLayout